-- Fury
local SPELL_BATTLE_SHOUT = Spell(6673, 10)
local SPELL_RECKLESSNESS = Spell(1719, 10)
local SPELL_RAMPAGE = Spell(184367)
local SPELL_DRAGON_ROAR = Spell(118000, 12)
local SPELL_BLOODTHIRST = Spell(23881)
local SPELL_EXECUTE = Spell(5308)
local SPELL_RAGING_BLOW = Spell(85288)
local SPELL_WHIRLWIND = Spell(190411, 8)
local SPELL_FURIOUS_SLASH = Spell(100130)
local SPELL_VICTORY_RUSH = Spell(34428)

local AURA_BATTLE_SHOUT = 6673
local AURA_VICTORIOUS = 32216
local AURA_WHIRLWIND = 85739
local AURA_ENRAGED_REGENERATION = 184364
local AURA_ENRAGED = 184362

local Fury = {}

function Fury.DoCombat(player, target)
	if not player:HasAura(AURA_BATTLE_SHOUT) and SPELL_BATTLE_SHOUT:CanCast() then
		SPELL_BATTLE_SHOUT:Cast(player)
		return
	end

	if player:HasAura(AURA_VICTORIOUS) and player.HealthPercent < 80 and SPELL_VICTORY_RUSH:CanCast(target) then
		SPELL_VICTORY_RUSH:Cast(target)
		return
	end

	if player:HasAura(AURA_ENRAGED_REGENERATION) and SPELL_BLOODTHIRST:CanCast(target) then
		SPELL_BLOODTHIRST:Cast(target)
		return
	end

	if #player:NearbyEnemyUnits(8) > 2 then
		Fury.AoERotation(player, target)
	else
		Fury.SingleRotation(player, target)
	end
end

function Fury.AoERotation(player, target)
	if not player:HasAura(AURA_WHIRLWIND) and SPELL_WHIRLWIND:CanCast(target) then
		SPELL_WHIRLWIND:Cast(player)
		return
	end

	Fury.SingleRotation(player, target)
end

function Fury.SingleRotation(player, target)
	if player:DistanceTo(Me.Target) < 12 then
		if SPELL_RECKLESSNESS:CanCast(target) then
			SPELL_RECKLESSNESS:Cast(player)
			return
		end

		if SPELL_DRAGON_ROAR:CanCast(target) then
			SPELL_DRAGON_ROAR:Cast(player)
			return
		end
	end

	if target.HealthPercent < 20 and SPELL_EXECUTE:CanCast(target) then
		SPELL_EXECUTE:Cast(target)
		return
	end

	if ((not player:HasAura(AURA_ENRAGED) and player.RagePercent > 75) or player.RagePercent > 90) and SPELL_RAMPAGE:CanCast(target) then
		SPELL_RAMPAGE:Cast(target)
		return
	end

	if SPELL_BLOODTHIRST:CanCast(target) then
		SPELL_BLOODTHIRST:Cast(target)
		return
	end

	if SPELL_RAGING_BLOW:CanCast(target) then
		SPELL_RAGING_BLOW:Cast(target)
		return
	end

	if SPELL_FURIOUS_SLASH:CanCast(target) then
		SPELL_FURIOUS_SLASH:Cast(target)
		return
	end
end

return Fury