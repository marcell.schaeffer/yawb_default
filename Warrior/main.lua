require('Common.Shared')

local arms = require('Arms')
local fury = require('Fury')
local protection = require('Protection')

--function OnEnterWorld()
--end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 71 then
		arms.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 72 then
		fury.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 73 then
		protection.DoCombat(Me, Me.Target)
		return
	end
end