require('Common.Shared')

local havoc = require('Havoc')
local vengeance = require('Vengeance')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 577 then
		havoc.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 581 then
		vengeance.DoCombat(Me, Me.Target)
		return
	end
end