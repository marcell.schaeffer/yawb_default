require('Common.Shared')

local elemental = require('Elemental')
local enhancement = require('Enhancement')
local restoration = require('Restoration')

function OnEnterWorld()

end

function OnUpdateIG()
	if Me.SpecializationID == 264 then
		restoration.DoCombat(Me, Me.Target)
		return
	end
	
	if not ShouldAttackSpecial(Me, Me.Target) then
		return
	end
	if Me.SpecializationID == 263 then
		enhancement.DoCombat(Me, Me.Target)
		return
	end
	if not ShouldAttack(Me, Me.Target) then
		return
	end
	if Me.SpecializationID == 262 then
		elemental.DoCombat(Me, Me.Target)
		return
	end
end