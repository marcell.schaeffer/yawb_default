require('Common.Shared')

local affliction = require('Affliction')
local demonology = require('Demonology')
local destruction = require('Destruction')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 265 then
		affliction.DoCombat(Me, Me.Target)
		return
	end
	if Me.SpecializationID == 266 then
		demonology.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 267 then
		destruction.DoCombat(Me, Me.Target)
		return
	end
end