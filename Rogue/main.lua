require('Common.Shared')

local assassination = require('Assassination')
local outlaw = require('Outlaw')
local subletly = require('Subtlety')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 259 then
		assassination.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 260 then
		outlaw.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 261 then
		subletly.DoCombat(Me, Me.Target)
		return
	end
end