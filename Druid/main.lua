require('Common.Shared')

local balance = require('Balance')
local feral = require('Feral')
local guardian = require('Guardian')
local restoration = require('Restoration')

function OnEnterWorld()

end

function OnUpdateIG()
	if Me.SpecializationID == 105 then
		restoration.DoCombat(Me, Me.Target)
		return
	end

	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 102 then
		balance.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 103 then
		feral.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 104 then
		guardian.DoCombat(Me, Me.Target)
		return
	end
end