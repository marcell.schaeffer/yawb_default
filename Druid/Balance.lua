-- TODO
-- Implement talents
-- Implement defensives
-- Implement heals

local SPELL_MOONFIRE = Spell(8921)
local SPELL_SUNFIRE = Spell(93402)
local SPELL_STELLAR_FLARE = Spell(202347)
local SPELL_STARSURGE = Spell(78674)
local SPELL_LUNAR_STRIKE = Spell(194153)
local SPELL_SOLAR_WRATH = Spell(190984)
local SPELL_STARFALL = Spell(191034)
local SPELL_BARKSKIN = Spell(22812)
local SPELL_ENTANGLING_ROOTS = Spell(339)
local SPELL_HIBERNATE = Spell(2637)
local SPELL_CELESTIAL_ALIGNMENT = Spell(194223)
local SPELL_INNERVATE = Spell(29166)
local SPELL_SOLAR_BEAM = Spell(78675)
local SPELL_REGROWTH = Spell(8936)
local SPELL_SOOTHE = Spell(2908)
local SPELL_REMOVE_CORRUPTION = Spell(2782)

local AURA_MOONFIRE = 164812
local AURA_SUNFIRE = 164815
local AURA_LUNAR_EMPOWERMENT = 164547
local AURA_SOLAR_EMPOWERMENT = 164545
local AURA_STELLAR_FLARE = 202347

local Balance = {}

function Balance.DoCombat(player, target)
	if #target:NearbyEnemyUnits(12) > 3 then
		Balance.AoERotation(player, target)
	else
		Balance.SingleRotation(player, target)
	end
end

function Balance.AoERotation(player, target)
	Balance.SingleRotation(player, target)
end

function Balance.SingleRotation(player, target)
	local units = player:NearbyEnemyUnits(40)
	local players = player:NearbyEnemyPlayers(40)

	-- This doesnt work on dummies because they are not considered InCombat.
	-- Need to check LOS!!!
	for i = 1, #units do
		if units[i].InCombat and player:IsFacing(units[i].Position) then
			if not units[i]:HasAuraByPlayer(AURA_MOONFIRE) and SPELL_MOONFIRE:CanCast(units[i]) then
				SPELL_MOONFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_SUNFIRE) and SPELL_SUNFIRE:CanCast(units[i]) then
				SPELL_SUNFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_STELLAR_FLARE) and SPELL_STELLAR_FLARE:CanCast(units[i]) then
				SPELL_STELLAR_FLARE:Cast(units[i])
				return
			end
		end
	end

	for i = 1, #players do
		if players[i].InCombat then
			if not players[i]:HasAuraByPlayer(AURA_MOONFIRE) and SPELL_MOONFIRE:CanCast(players[i]) then
				SPELL_MOONFIRE:Cast(players[i])
				return
			end

			if not players[i]:HasAuraByPlayer(AURA_SUNFIRE) and SPELL_SUNFIRE:CanCast(players[i]) then
				SPELL_SUNFIRE:Cast(players[i])
				return
			end

			if not players[i]:HasAuraByPlayer(AURA_STELLAR_FLARE) and SPELL_STELLAR_FLARE:CanCast(players[i]) then
				SPELL_STELLAR_FLARE:Cast(players[i])
				return
			end
		end
	end

	local solarEmp = player:Aura(AURA_SOLAR_EMPOWERMENT)
	local lunarEmp = player:Aura(AURA_LUNAR_EMPOWERMENT)
	if (solarEmp == nil or solarEmp.Stack < 3) and (lunarEmp == nil or lunarEmp.Stack < 3) and SPELL_STARSURGE:CanCast(target) and player.AstralPower > 40 then
		SPELL_STARSURGE:Cast(target)
		return
	end

	if lunarEmp ~= nil and lunarEmp.Stack > 0 and SPELL_LUNAR_STRIKE:CanCast(target) then
		SPELL_LUNAR_STRIKE:Cast(target)
	end

	if solarEmp ~= nil and solarEmp.Stack > 0 and SPELL_SOLAR_WRATH:CanCast(target) then
		SPELL_SOLAR_WRATH:Cast(target)
	end

	if SPELL_SOLAR_WRATH:CanCast(target) then
		SPELL_SOLAR_WRATH:Cast(target)
	end
end

return Balance