require('Common.Shared')

local blood = require('Blood')
local frost = require('Frost')
local unholy = require('Unholy')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 250 then
		blood.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 251 then
		frost.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 252 then
		unholy.DoCombat(Me, Me.Target)
		return
	end
end