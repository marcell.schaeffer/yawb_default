require('Common.Shared')

local arcane = require('Arcane')
local fire = require('Fire')
local frost = require('Frost')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 62 then
		arcane.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 63 then
		fire.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 64 then
		frost.DoCombat(Me, Me.Target)
		return
	end
end