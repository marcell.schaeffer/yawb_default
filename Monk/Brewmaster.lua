local SPELL_TIGER_PALM = Spell(100780)
local SPELL_BLACKOUT_STRIKE = Spell(205523)
local SPELL_CHI_WAVE = Spell(115098)
local SPELL_KEG_SMASH = Spell(121253)
local SPELL_BREATH_OF_FIRE = Spell(115181, 8)
local SPELL_SPEAR_HAND_STRIKE = Spell(116705, 8)
local SPELL_EXPEL_HARM = Spell(115072, 8)
local SPELL_RUSHING_JADE_WIND = Spell(116847, 18)
local SPELL_CHI_BURST = Spell(123986, 10)
local SPELL_FORTIFYING_BREW = Spell(115203, 100)
local SPELL_DETOX = Spell(218164)
local SPELL_VIVIFY = Spell(116670)

local SPELL_IRONSKIN_BREW = Spell(115308,100)
local SPELL_PURIFYING_BREW = Spell(119582,100)

local AURA_IRONSKINBREW = 215479
local AURA_MEDSTAGGER = 124274
local AURA_HIGHSTAGGER = 124273
local AURA_RUSHINGJADEWIND = 116847

local Brewmaster = {}	

function Brewmaster.DoCombat(player, target)
	if #player:NearbyEnemyUnits(10) > 1 then
		Brewmaster.AoERotation(player, target)
	else
		Brewmaster.SingleRotation(player, target)
	end
end

function Brewmaster.AoERotation(player, target)
	Brewmaster.SingleRotation(player, target)
end

function Brewmaster.SingleRotation(player, target)
	local Draw = TextHook.Instance()
	local powers = player:Powers()
	local energy = powers[2]
	local farligt = player.Debuffs

	local medstagger = player:HasAura(AURA_MEDSTAGGER)
	local highstagger = player:HasAura(AURA_HIGHSTAGGER)
	local brewexists = player:HasAura(AURA_IRONSKINBREW)
	local brewbuff = player:AuraByPlayer(AURA_IRONSKINBREW)
	local jadeexists = player:HasAura(AURA_RUSHINGJADEWIND)
	local rushingjade = player:AuraByPlayer(AURA_RUSHINGJADEWIND)


	local units = player:NearbyEnemyUnits(10)
	local brewtimer = 0
	local jadetimer = 0

	if jadeexists then
		jadetimer = rushingjade.Timeleft
	end

	if brewexists then
		brewtimer = brewbuff.Timeleft
	end

	if SPELL_FORTIFYING_BREW:CanCast() and player.HealthPercent < 25 then
		SPELL_FORTIFYING_BREW:Cast(player)
		return
	end

	for i = 1, #farligt do
		if farligt[i].Type == SpellType.Poison or farligt[i].Type == SpellType.Disease then
			if SPELL_DETOX:CanCast(player) then
				SPELL_DETOX:Cast(player)
			end
		end
	end

	if SPELL_VIVIFY:CanCast(player) and not player.InCombat and player.HealthPercent < 85 and not player.IsMoving then
		SPELL_VIVIFY:Cast(player)
		return
	end

	if not ShouldAttack(Me, Me.Target) then
		return
	end

	for i = 1, #units do
		if units[i].InCombat and Brewmaster.ShouldInterrupt(units[i]) then
			if SPELL_SPEAR_HAND_STRIKE:CanCast(units[i]) then
				SPELL_SPEAR_HAND_STRIKE:Cast(units[i])
				return
			end
		end
	end

	if SPELL_EXPEL_HARM:CanCast(target) and player.HealthPercent <= 70 then
		SPELL_EXPEL_HARM:Cast(target)
		return
	end

	if SPELL_PURIFYING_BREW:CanCast(target) and highstagger and brewtimer > 6000 then
		SPELL_PURIFYING_BREW:Cast(player)
		return
	end

	if SPELL_IRONSKIN_BREW:CanCast(target) and (SPELL_IRONSKIN_BREW.Charges > 1 or not brewexists) and brewtimer <= 14000 then
		SPELL_IRONSKIN_BREW:Cast(player)
		return
	end

	if SPELL_RUSHING_JADE_WIND:CanCast(target) and jadetimer < 2000 then
		SPELL_RUSHING_JADE_WIND:Cast(target)
		return
	end 

	if SPELL_CHI_BURST:CanCast(target) and not SPELL_KEG_SMASH.IsReady and not player.IsMoving then
		SPELL_CHI_BURST:Cast(target)
		return
	end

	if SPELL_KEG_SMASH:CanCast(target) then
		SPELL_KEG_SMASH:Cast(target)
		return
	end

	if SPELL_BLACKOUT_STRIKE:CanCast(target) and SPELL_KEG_SMASH.IsCoolingDown then
		SPELL_BLACKOUT_STRIKE:Cast(target)
		return
	end

	if SPELL_BREATH_OF_FIRE:CanCast(target) and not player.IsMoving and not SPELL_KEG_SMASH:CanCast(target) then
		SPELL_BREATH_OF_FIRE:Cast(target)
		return
	end

	if SPELL_TIGER_PALM:CanCast(target) and SPELL_KEG_SMASH.IsCoolingDown and energy >= 65 then
		SPELL_TIGER_PALM:Cast(target)
		return
	end
end

function Brewmaster.ShouldInterrupt(target)
	if target.IsCasting or target.IsChanneling then
		local sprop = target.SpellInfo
		local timesup = sprop.Timeleft
    	if sprop ~= nil then
			if sprop.Interruptible and timesup <= 400 then
                return true -- if interruptable
        end
        end
    end
    return false
end

return Brewmaster