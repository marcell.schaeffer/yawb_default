require('Common.Dispell')

-- Spells
local SPELL_VIVIFY = Spell(116670)
local SPELL_RENEWING_MIST = Spell(115151)
local SPELL_ESSENCE_FONT = Spell(191837, 100)
local SPELL_TIGER_PALM = Spell(100780)
local SPELL_BLACKOUT_KICK = Spell(100784)
local SPELL_RISING_SUN_KICK = Spell(107428)
local SPELL_SPINNING_CRANE_KICK = Spell(101546)
local SPELL_FORTIFYING_BREW = Spell(243435, 100)
local SPELL_LIFE_COCOON = Spell(116849)
local SPELL_DETOX = Spell(115450)
local SPELL_ENEVELOPING_MIST = Spell(124682)
local SPELL_SOOTHING_MIST = Spell(115175)
local SPELL_JADE_SERPENT_STATUE = Spell(115313, 100)
local SPELL_HEALING_ELIXIR = Spell(122281, 100)
local SPELL_THUNDER_FOCUS_TEA = Spell(116680, 100)
local SPELL_REVIVAL = Spell(115310, 100)

local AURA_RENEWING_MIST = 119611
local AURA_ENVELOPING_MIST = 124682
local AURA_SOOTHING_MIST = 198533

local STATUE_ID = 60849

local Settings =
{
	DamageManaPercentAbove = 25,
	-- Higher value = more healing
	OutOfCombatMultipler = 0.90, -- Make this higher on grievous weeks.
	HealMultiplier = 1.19, -- Lower if it seems to spam too much healing, increase if tank is Demon hunter.
	ManaMultiplier = 0.30,
	RaidMultiplier = 0.60 
}

local Mistweaver = {}


function Mistweaver.DoCombat(player, target)
	local sprop = player.SpellInfo

	if player.IsDead or
		player.IsMounted or
		player.HasTerrainSpellActive or
		sprop ~= nil and sprop.SpellId == 191837 then
		return
	end

	-- Healing
	local healTargets = Mistweaver.FindHealingTargets(player)
	local numHealTargets = 0
	local CastEssenceFont = false
	local CastRevival = false
	local SOOTHING_MISTUp = false
	local recastSOOTHING_MIST = true
	local foundTank = nil

	local statueup = false

	local GetStatue = player:NearbyFriendlyUnits(40)

	for i = 1, #GetStatue do
		if GetStatue[i].Entry == STATUE_ID then
			statueup = true
		end
	end

	-- First pass (set variables and such)
	for unit, score in spairs(healTargets, function(t, a, b) return t[b] < t[a] end) do
		local SOOTHING_MIST = unit:Aura(AURA_SOOTHING_MIST)

		-- Check RENEWING_MIST. This is used if there are not tanks around
		if SOOTHING_MIST ~= nil then
			SOOTHING_MISTUp = true
			if score < 25 then
				recastSOOTHING_MIST = true
			end
		end

		-- Check if unit is tank
		if unit.GroupRole == Roles.Tank then
			foundTank = unit
		end

		-- Essence font
		if score > 35 then
			numHealTargets = numHealTargets + 1
		end

		-- Dispell, find a new place for this?
		if ShouldDispell(unit) and SPELL_DETOX:CanCast(unit) then
			SPELL_DETOX:Cast(unit)
			return
		end
	end

	if ((player.InRaid and numHealTargets > 4) or numHealTargets > 2) and SPELL_ESSENCE_FONT:CanCast() then
		CastEssenceFont = true
	end

	if ((player.InRaid and numHealTargets > 8) or numHealTargets > 4) and SPELL_ESSENCE_FONT:CanCast() then
		CastRevival = true
	end

	-- Second pass (do healing)
	for unit, score in spairs(healTargets, function(t, a, b) return t[b] < t[a] end) do
		local renew = unit:AuraByPlayer(AURA_RENEWING_MIST)
		local envelop = unit:AuraByPlayer(AURA_ENVELOPING_MIST)

		if not statueup and score > 10 and SPELL_JADE_SERPENT_STATUE:CanCast() and player.InCombat then
			SPELL_JADE_SERPENT_STATUE:CastOnGround(player.Position)
			return
		end

		-- healing elixir
		if player.HealthPercent < 40 and SPELL_HEALING_ELIXIR:CanCast() then
			SPELL_HEALING_ELIXIR:Cast(player)
			return
		end

		-- FORTIFYING_BREW
		if player.HealthPercent < 50 and SPELL_FORTIFYING_BREW:CanCast() then
			SPELL_FORTIFYING_BREW:Cast(player)
			return
		end

		if SPELL_THUNDER_FOCUS_TEA:CanCast() and player.InCombat then
			SPELL_THUNDER_FOCUS_TEA:Cast(player)
			return
		end

		-- LIFE_COCOON
		if score > 90 and SPELL_LIFE_COCOON:CanCast(unit) then
			SPELL_LIFE_COCOON:Cast(unit)
			return
		end

		if CastRevival and SPELL_REVIVAL:CanCast(unit) then
			SPELL_REVIVAL:Cast(unit)
			return
		end

		-- Essence font
		if CastEssenceFont and SPELL_ESSENCE_FONT:CanCast(unit) then
			SPELL_ESSENCE_FONT:Cast(unit)
			return
		end
		
		-- SOOTHING
		if not SOOTHING_MISTUp then
			if foundTank ~= nil and SPELL_SOOTHING_MIST:CanCast(foundTank) and foundTank.InCombat and statueup then
				SPELL_SOOTHING_MIST:Cast(foundTank)
				return
			elseif foundTank == nil and recastSOOTHING_MIST and score > 30 and SPELL_SOOTHING_MIST:CanCast(unit) and unit.InCombat and statueup then
				SPELL_SOOTHING_MIST:Cast(unit)
				return
			end
		end

		if (score > 25 or SPELL_RENEWING_MIST.Charges > 0) and renew == nil and SPELL_RENEWING_MIST:CanCast(unit) and player.InCombat then
			SPELL_RENEWING_MIST:Cast(unit)
			return
		end

		
		if score > 45 and SPELL_ENEVELOPING_MIST:CanCast(unit) and envelop == nil and not player.isMoving then
			SPELL_ENEVELOPING_MIST:Cast(unit)
			return
		end

		-- VIVIFY
		if not player.IsMoving and score > 45 and SPELL_VIVIFY:CanCast(unit) then
			SPELL_VIVIFY:Cast(unit)
			return
		end
	end

	-- Only continue if we should attack and mana percentage is over 70%
	if not ShouldAttack(player, target) then
		return
	end
	
	-- Damage

	if #player:NearbyEnemyUnits(8) > 3 and SPELL_SPINNING_CRANE_KICK:CanCast(target) then
		SPELL_SPINNING_CRANE_KICK:Cast(target)
		return
	end

	if not SPELL_BLACKOUT_KICK:CanCast(target) and not SPELL_RISING_SUN_KICK:CanCast(target) and SPELL_TIGER_PALM:CanCast(target) then
		SPELL_TIGER_PALM:Cast(target)
		return
	end

	if SPELL_RISING_SUN_KICK:CanCast(target) and player.ManaPercent > Settings.DamageManaPercentAbove then
		SPELL_RISING_SUN_KICK:Cast(target)
		return
	end

	if SPELL_BLACKOUT_KICK:CanCast(target) and not SPELL_RISING_SUN_KICK:CanCast(target) then
		SPELL_BLACKOUT_KICK:Cast(target)
		return
	end

	
end

function Mistweaver.FindHealingTargets(player)
	local ret = {}
	local nearby = player:NearbyFriendlyPlayers(40)
	table.insert(nearby, player)

	for i = 1, #nearby do
		local score = 0

		if nearby[i].InParty or nearby[i].InRaid then
			score = score + 10

			if nearby[i].GroupRole == Roles.Tank then
				score = score + 20
			elseif nearby[i].GroupRole == Roles.Damage then
				score = score + 10
			elseif nearby[i].GroupRole == Roles.Healer then
				score = score + 10
			end
		end

		score = score + (100 - nearby[i].HealthPercent)

		if not player:HasAura(AURA_INNERVATE) then
			score = score - ((100 - player.ManaPercent) * Settings.ManaMultiplier)
		else
			score = score + 40
		end

		if player.InRaid then
			score = score * Settings.RaidMultiplier
		end

		if not nearby[i].InCombat then
			score = score * Settings.OutOfCombatMultipler
		end

		score = score * Settings.HealMultiplier

		if score > 0 or nearby[i].InCombat then
			ret[nearby[i]] = score
		end
	end

	return ret
end

return Mistweaver