require('Common.Shared')

local brewmaster = require('Brewmaster')
local windwalker = require('Windwalker')
local mistweaver = require('Mistweaver')

function OnEnterWorld()

end

function OnUpdateIG()

	if Me.SpecializationID == 270 then
		mistweaver.DoCombat(Me, Me.Target)
		return
	end
	if Me.SpecializationID == 268 then
		brewmaster.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 269 then
		windwalker.DoCombat(Me, Me.Target)
		return
	end
	if not ShouldAttack(Me, Me.Target) then
		return
	end

end