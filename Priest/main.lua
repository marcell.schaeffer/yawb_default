require('Common.Shared')

local discipline = require('Discipline')
local holy = require('Holy')
local shadow = require('Shadow')

function OnEnterWorld()

end

function OnUpdateIG()
	if Me.SpecializationID == 256 then
		discipline.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 258 then
		shadow.DoCombat(Me, Me.Target)
		return
	end
	
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 257 then
		holy.DoCombat(Me, Me.Target)
		return
	end
end