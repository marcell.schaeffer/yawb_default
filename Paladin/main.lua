require('Common.Shared')

local holy = require('Holy')
local protection = require('Protection')
local retribution = require('Retribution')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 65 then
		holy.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 66 then
		protection.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 70 then
		retribution.DoCombat(Me, Me.Target)
		return
	end
end