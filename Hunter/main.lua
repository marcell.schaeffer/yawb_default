require('Common.Shared')

local beastmastery = require('Beastmastery')
local marksmanship = require('Marksmanship')
local survival = require('Survival')

function OnEnterWorld()

end

function OnUpdateIG()
	if not ShouldAttack(Me, Me.Target) then
		return
	end

	if Me.SpecializationID == 253 then
		beastmastery.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 254 then
		marksmanship.DoCombat(Me, Me.Target)
		return
	elseif Me.SpecializationID == 255 then
		survival.DoCombat(Me, Me.Target)
		return
	end
end